using UnityEngine;
using System.Collections.Generic;
using System;

public class VectorVL {
	
	public float[] values = null;
	
	public int size {
		get {
			return values.Length;
		}
	}
	
	public VectorVL (int size) {
		values = new float[size];
	}

	public VectorVL (float[] values) {
		this.values = values;
	}
	
	public float this [int index] {
		get {
			return values[index];
		}
		set {
			values[index] = value;
		}
	}
	
	public static VectorVL operator+ (VectorVL left, VectorVL right) {
		int size = left.size;
		
		VectorVL result = new VectorVL(size);
		if (size != right.size) {
			Debug.Log ("Invalid length.");
			return result;
		}
		
		for (int i = 0; i < size; ++ i) {
			result[i] = left[i] + right[i];
		}
		
		return result;
	}
	
	public static VectorVL operator- (VectorVL left, VectorVL right) {
		int size = left.size;
		
		VectorVL result = new VectorVL(size);
		if (size != right.size) {
			Debug.Log ("Invalid length.");
			return result;
		}
		
		for (int i = 0; i < size; ++ i) {
			result[i] = left[i] - right[i];
		}
		
		return result;
	}
	
	public static VectorVL operator* (float s, VectorVL vec) {
		int size = vec.size;
		
		VectorVL result = new VectorVL(size);
		
		for (int i = 0; i < size; ++ i) {
			result[i] = s * vec[i];
		}
		
		return result;
	}

}

