using System.Collections.Generic;
using UnityEngine;

namespace ViewDependentRodSimulation {
	[System.Serializable]
	public class Vector3List {
		public List<Vector3> vList;
		
		public Vector3List() {
			vList = new List<Vector3>();
		}
	}
}