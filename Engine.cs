using UnityEngine;
using System.Collections.Generic;

namespace ViewDependentRodSimulation {
	
public class Engine : MonoBehaviour {
	
	private	float	deltaTime;
	public	float	gravity		=	-2.0f;
	public	int		stiffness	=	4;
	
	// for demo program
	public bool switchOff = false;

	private	List<BaseObject> objects = null;
	
	// Use this for initialization
	void Start () {
		deltaTime = Time.fixedDeltaTime;
		
		objects = new List<BaseObject>();
		
		int nChildren = transform.GetChildCount();
		for (int i = 0; i < nChildren; ++ i) {
			Transform t = transform.GetChild(i);
			BaseObject physicsBase = t.GetComponent(typeof(BaseObject)) as BaseObject;
			if (physicsBase != null) {
				objects.Add(physicsBase);
			}
		}
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		SimulateStep(deltaTime);
		FinalizeUpdate();
	}
	
	void SimulateStep(float dt) {
		CalculateForces(dt);
		CalculateVelocities(dt);
		DampVelocities(dt);
		CalculateEstimatedPositions(dt);
		GenerateCollisionConstraints(dt);
		ProjectConstraints(dt, stiffness);
		ApplyEstimatedPositions(dt);
		CorrectVelocities(dt);
	}
	
	void CalculateForces(float dt) {
		foreach (BaseObject o in objects) {
			o.CalculateForces(dt);
		}
	}
	
	void CalculateVelocities(float dt) {
		foreach (BaseObject o in objects) {
			o.CalculateVelocities(dt);
		}
	}
	
	void DampVelocities(float dt) {
		foreach (BaseObject o in objects) {
			o.DampVelocities(dt);
		}
	}
	
	void CalculateEstimatedPositions(float dt) {
		foreach (BaseObject o in objects) {
			o.CalculateEstimatedPositions(dt);
		}
	}
	
	void GenerateCollisionConstraints(float dt) {
		// In Unity, collision handlings are not here...
	}

	void ProjectConstraints(float dt, int stiffness) {
		foreach (BaseObject o in objects) {
			o.ProjectConstraints(dt, stiffness);
		}
	}
	
	void ApplyEstimatedPositions(float dt) {
		foreach (BaseObject o in objects) {
			o.ApplyEstimatedPositions(dt);
		}
	}
	
	void CorrectVelocities(float dt) {
		foreach (BaseObject o in objects) {
			o.CorrectVelocities(dt);
		}
	}
	
	void FinalizeUpdate() {
		foreach (BaseObject o in objects) {
			o.FinalizeUpdate();
		}
	}
}
	
}