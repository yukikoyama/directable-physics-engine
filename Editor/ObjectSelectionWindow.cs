#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections;

public class ObjectSelectionWindow : EditorWindow {

	const int width		= 300;
	const int height	= 200;
	const int x			= 100;
	const int y			= 100;

	private	GameObject	fbxObject	= null;
	private	int			nRods		= 1;
	
	[MenuItem ("View-Dependent Rods/Convert FBX Object")]
	public static ObjectSelectionWindow Init() {
		ObjectSelectionWindow window = EditorWindow.GetWindow<ObjectSelectionWindow>("Rod") as ObjectSelectionWindow;

		Rect pos = window.position;
		pos.height	= ObjectSelectionWindow.height;
		pos.width 	= ObjectSelectionWindow.width;
		pos.x		= ObjectSelectionWindow.x;
		pos.y		= ObjectSelectionWindow.y;
		window.position = pos;
		
		return window;
	}
	
	void OnGUI() {
		
		// layout
		EditorGUILayout.BeginVertical();
		EditorGUILayout.Space();
		EditorGUILayout.LabelField("Please select an FBX object which has");
		EditorGUILayout.LabelField("  more than one rod.");
		fbxObject = EditorGUILayout.ObjectField("FBX object", fbxObject, typeof(GameObject), true) as GameObject;
		nRods = EditorGUILayout.IntField("Number of rods", nRods);
		GUILayout.FlexibleSpace();
		EditorGUILayout.BeginHorizontal();
		bool cancelButton	= GUILayout.Button("Cancel");
		bool okButton		= GUILayout.Button("OK");
		EditorGUILayout.EndHorizontal();
		EditorGUILayout.Space();
		EditorGUILayout.EndVertical();
		
		// cancel button event
		if (cancelButton) {
			Close ();
		}
		
		// ok button event
		if (okButton) {
		
			// check the fbxObject
			if (fbxObject != null) {
				
				// check the fbxObject's internal structure
				bool isFbx = true;		// todo
				
				if (isFbx) {
					// show the next window
					RodSelectionWindow window = RodSelectionWindow.Init();
					window.Initialize(fbxObject, nRods);
					
				} else {
					ErrorWindow window = ErrorWindow.Init();
					window.position = this.position;
					window.message = "Selected object has an invalid structure.";
				}
			} else {
				ErrorWindow window = ErrorWindow.Init();
				window.position = this.position;
				window.message = "Please select an FBX object.";

			}
			// exit
			Close();
		}
	}
}
#endif
