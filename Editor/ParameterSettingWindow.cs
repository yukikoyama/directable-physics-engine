#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

public class ParameterSettingWindow : EditorWindow {
	
	private	GameObject		fbxObject;
	private	int				nRods			= 1;
	private	List<Transform>	allTransforms	= null;
	private	int[]			roots			= null;
	private	int[]			attaches		= null;
	
	private	string[]		names 			= null;
	private Vector2			scrollPosition;
	private int				rootJoint		= 1;

	private bool	attachCollidersToRods	= true;
	private float	collidersRadius			= 0.05f;
	
	private bool	attachCollidersToBody	= true;
	private float	capsuleSize				= 0.05f;

	private int		nAnimatedParticles		= 3;

	public static ParameterSettingWindow Init() {
		ParameterSettingWindow window = EditorWindow.GetWindow<ParameterSettingWindow>("Rod") as ParameterSettingWindow;
		
		return window;
	}
	
	void OnGUI() {
		
		GUIStyle style = new GUIStyle();
		style.fontStyle = FontStyle.Bold;
	
		// layout
		EditorGUILayout.BeginVertical();
		
		attachCollidersToRods = EditorGUILayout.BeginToggleGroup ("Attach colliders to rods", attachCollidersToRods);
		collidersRadius = EditorGUILayout.Slider ("Radius", collidersRadius, 0.0f, 1.0f);
		EditorGUILayout.EndToggleGroup();
		
		EditorGUILayout.Separator();
		
		attachCollidersToBody = EditorGUILayout.BeginToggleGroup ("Attach colliders to body", attachCollidersToBody);
		capsuleSize = EditorGUILayout.Slider ("Radius", capsuleSize, 0.0f, 1.0f);

		string s = "Please select the joint root.";
		GUILayout.Label(s, style);
		scrollPosition = GUILayout.BeginScrollView(scrollPosition);
		rootJoint = GUILayout.SelectionGrid(rootJoint, names, 7);
		GUILayout.EndScrollView();

		EditorGUILayout.EndToggleGroup();
		
		EditorGUILayout.Separator();

		EditorGUILayout.LabelField("Number of partially-kinematic joints", style);
		nAnimatedParticles = EditorGUILayout.IntField(nAnimatedParticles);
	
		GUILayout.FlexibleSpace();
		bool button = GUILayout.Button("OK");
		EditorGUILayout.Space();
		EditorGUILayout.EndVertical();
		
		// button event
		if (button) {
			StructureGenerationScript script = new StructureGenerationScript();
			script.fbxObject 				= fbxObject;
			script.nRods					= nRods;
			script.allTransforms			= allTransforms;
			script.roots					= roots;
			script.attaches					= attaches;
			script.attachCollidersToRods	= attachCollidersToRods;
			script.colliderRadius			= collidersRadius;
			script.attachCollidersToBody	= attachCollidersToBody;
			script.capsuleSize				= capsuleSize;
			script.nAnimatedParticles		= nAnimatedParticles;
			script.rootJoint				= rootJoint;
			bool success = script.Execute();
		
			if (success) {
				Debug.Log("Succeeded.");
			} else {
				Debug.Log("Failed");
			}
			
			Close();
		}
	}
	
	public void Initialize(GameObject fbxObject , int nRods, List<Transform> allTransforms, int[] roots, int[] attaches, string[] names) { 
		this.fbxObject	= fbxObject;
		this.nRods		= nRods;
		this.allTransforms	= allTransforms;
		this.roots		= roots;
		this.attaches	= attaches;
		this.names		= names;
	}
}
#endif
