using UnityEngine;
using System.Collections.Generic;

namespace ViewDependentRodSimulation {
	
	public class ViewDependentRodObject : SuppressedRodObject {
		
		[SerializeField] private ViewDependenceManager manager;
		public	Camera		targetCamera;
		public	Transform	rootTransform;
		
		public override void InitializeEditor() {
			targetCamera = GameObject.FindWithTag ("MainCamera").camera;
			manager = new ViewDependenceManager();
			
			base.InitializeEditor();
		}
		
		// from inspector
		public override void AddPoseEditor() {
			base.AddPoseEditor();
			
			foreach (AnimatedParticle ap in animParticles) {
				ap.AddPoseEditor();
			}
			Vector3 x = (targetCamera.transform.position - rootTransform.position).normalized;
			QuaternionEx rot = QuaternionEx.Convert(rootTransform.rotation).inverse;
			Vector3 y = rot.RotateVector(x);
			
			manager.AddKeyDirection(y);
		}
		
		// from scene view
		public void AddPoseWithCameraPositionEditor(Vector3 position) {
			base.AddPoseEditor();
			
			foreach (AnimatedParticle ap in animParticles) {
				ap.AddPoseEditor();
			}
			
			Vector3 x = (position - rootTransform.position).normalized;
			QuaternionEx rot = QuaternionEx.Convert(rootTransform.rotation).inverse;
			Vector3 y = rot.RotateVector(x);
			
			manager.AddKeyDirection(y);
		}
		
		public override void DeletePoseEditor() {
			if (manager.nKeyDirections <= 1) {
				Debug.Log("Nothing to do.");
				return;
			}

			base.DeletePoseEditor();
			
			foreach (AnimatedParticle ap in animParticles) {
				ap.DeletePoseEditor();
			}
			
			manager.DeleteKeyDirection();
		}
		
		/************************************************************************/
		
		protected override void Initialize() {
			base.Initialize();
		}
		
		protected override void UpdateWeights() {
			if (nPoses == 1) {
				weights[0]            = 1.0f;
				minWeights[0]         = 1.0f;
				maxWeights[0]         = 1.0f;
				weightsForVelocity[0] = 1.0f;
				return;
			}
			
			// for demo program
			if (engine.switchOff) {
				weights[0]            = 1.0f;
				minWeights[0]         = 1.0f;
				maxWeights[0]         = 1.0f;
				weightsForVelocity[0] = 1.0f;
				
				for (int i = 1; i < nPoses; ++ i) {
					weights[i]				= 0.0f;
					minWeights[i]			= 0.0f;
					maxWeights[i]			= 0.0f;
					weightsForVelocity[i]	= 0.0f;
				}
				
				return;				
			}
			
		    // update weights for update of position
			Vector3 currentViewERotation = GetCurrentViewRotation();
			weights = manager.GetWeights(currentViewERotation);
		}
		
		private Vector3 GetCurrentViewRotation() {
			Vector3 x = (targetCamera.transform.position - rootTransform.position).normalized;
			QuaternionEx rot = QuaternionEx.Convert(rootTransform.rotation).inverse;
			Vector3 y = rot.RotateVector(x);
			return y;
		}
	}
	
}