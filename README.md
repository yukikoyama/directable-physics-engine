# このソースコードについて
このソースコードは「こだわり物理エンジン」の本体となります。以下の学術論文の手法が実装されています。

*Yuki Koyama and Takeo Igarashi. 2013. View-dependent control of elastic rod simulation for 3D character animation. In Proceedings of the 12th ACM SIGGRAPH/Eurographics Symposium on Computer Animation (SCA '13). ACM, New York, NY, USA, 73-78.*

ソースコードの管理は小山裕己 (<koyama@is.s.u-tokyo.ac.jp>) が行っています。

Pull Request は強く歓迎します。

# プロジェクトページ
<http://www-ui.is.s.u-tokyo.ac.jp/~koyama/mitoh/index-j.html>

# ライセンス
このソースコードは **MIT License** の元で公開されています。

商用利用等は制限されていませんが、開発者に一報頂けると幸いです。

*Directable Physics Engine / ThirdParty / MatrixDecompositionProgram.cs*

の 1 ファイルに関してのみ、MICROSOFT PUBLIC LICENSE (MS-PL) の元で公開されています。

# その他
1. このソースコードの実装は上記の論文とは細部において内容が異なっている部分があります。
1. このソースコードは元々は基礎研究のためのプロトタイプであり、広くテストされているものではありません。
1. このソースコードは Unity scripts として書かれています (Unity 4.1.3 でのみ動作確認) 。その他の環境はサポートしていません。

# 使い方
このソースコードは Unity で使用可能です。まずは以下の Unity サンプルプロジェクトを見て下さい：

<http://www-ui.is.s.u-tokyo.ac.jp/~koyama/mitoh/unity_project.zip>

また、以下に簡単なチュートリアル動画を用意しています。

<http://youtu.be/DGsuveSPWPM>

より詳細な情報を知りたい場合は、開発者に問い合わせるか、ソースコードを読んで下さい。(ドキュメントを用意できていなくて申し訳ありません。)
