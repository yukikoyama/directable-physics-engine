using UnityEngine;
using System.Collections.Generic;

namespace ViewDependentRodSimulation {
	
	public class FKRodObject : RodObject {
		
		// for precomputation in converting
		[SerializeField] private List<Vector3>		positionVectorEditor	= null;
		[SerializeField] private List<Quaternion>	orientationVectorEditor = null;
				
		// arbitrary normal vector (in this case, (1.0f, 0.0f, 0.0f))
		[SerializeField] protected	Vector3	referenceDirection;
		// arbitrary vector	(in this case, (0.0f, 0.0f, 0.0f))
		[SerializeField] protected	Vector3	referencePosition;
			
		public override void InitializeEditor() {
			base.InitializeEditor();
			referenceDirection = Vector3.zero;
			referencePosition  = Vector3.zero;
			referenceDirection.x = 1.0f;
			RegisterBindPoseEditor();
		}
		
		protected void RegisterBindPoseEditor() {
			int nParticles = particles.Count;
			
			positionVectorEditor			= new List<Vector3>();
			orientationVectorEditor			= new List<Quaternion>();
			for (int index = 0; index < nParticles; ++ index) {
				OrientedParticle p = particles[index];
				Vector3    pos = p.transform.localPosition;
				Quaternion ori = p.transform.localRotation;
				ori = checkQuaternion(ori);
				
				positionVectorEditor.Add(pos);
				orientationVectorEditor.Add(ori);
			}
		}
		
		protected	List<float>			lengthVector		= null;
		protected	List<QuaternionEx>	rotationVector		= null;
		
		// This is required for skinning.
		protected	List<QuaternionEx>	orientationVector	= null;


		protected override void Initialize() {
			base.Initialize();
			RegisterBindPose();
		}
		
		protected void UpdateRestStateOfParticlesUsingFKParams() {
			Vector3 dir = referenceDirection;
			Vector3 pos = referencePosition;
			
			int nParticles = particles.Count;
			for (int i = 0; i < nParticles - 1; ++ i) {
				OrientedParticle	p = particles		[i];
				float				l = lengthVector	[i];
				QuaternionEx		q = rotationVector	[i].normalize;

				p.x0 = pos;
				p.q0 = orientationVector[i].normalize;
				
				Vector3 nextDir = q.RotateVector(dir).normalized;
				Vector3 nextPos = pos + nextDir * l;
				
				dir = nextDir;
				pos = nextPos;
			}
			{
				int i = nParticles - 1;
				OrientedParticle	p = particles		[i];
				p.x0 = pos;
				p.q0 = orientationVector[i].normalize;
			}
		}
		
		// This method should be called once before simulation.
		protected void RegisterBindPose() {
			lengthVector      = new List<float>();
			orientationVector = new List<QuaternionEx>();
			
			List<Vector3> tmpVector = new List<Vector3>();
			int nParticles = particles.Count;
			for (int index = 0; index < nParticles; ++ index) {
				OrientedParticle p = particles[index];

				Vector3 tmpv = p.transform.position;
				p.transform.localPosition = positionVectorEditor[index];
				Vector3 pos = p.transform.position;
				p.transform.position = tmpv;
				
				Quaternion tmpq = p.transform.rotation;
				p.transform.localRotation = orientationVectorEditor[index];
				Quaternion ori = p.transform.rotation;
				ori = checkQuaternion(ori);
				p.transform.rotation = tmpq;
				
				tmpVector.Add(pos);
				orientationVector.Add(QuaternionEx.Convert(ori));
			}
			
			for (int index = 0; index < nParticles - 1; ++ index) {
				Vector3 p0 = tmpVector[index + 0];
				Vector3 p1 = tmpVector[index + 1];
				
				float len = (p1 - p0).magnitude;
				
				lengthVector.Add(len);
			}
			lengthVector.Add(0.0f);		// dummy
		}

		// check quaternions for ensuring the interpolation goes along the shortest path
		static public QuaternionEx pivotQuaternion {
			get {
				return QuaternionEx.identity;
			}
		}

		protected static Quaternion checkQuaternion(Quaternion q) {
			QuaternionEx q2 = QuaternionEx.Convert(q);
			float dot = QuaternionEx.DotProduct(q2, pivotQuaternion);
			if (dot < 0.0f) {
				return (-1.0f * q2).convert;
			} else {
				return q;
			}
		}
		
		protected static QuaternionEx checkQuaternion(QuaternionEx q) {
			float dot = QuaternionEx.DotProduct(q, pivotQuaternion);
			if (dot< 0.0f) {
				return -1.0f * q;
			} else {
				return q;
			}
		}
	}
}