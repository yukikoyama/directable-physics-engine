using UnityEngine;
using System;
using System.Collections.Generic;

namespace ViewDependentRodSimulation {
	
	[Serializable]
	class ViewDependenceManager {
		
		public List<Vector3>	keyDirections = null;
		public List<float>		influences    = null;
		
		public int nKeyDirections {
			get {
				return keyDirections.Count;
			}
		}
		
		public ViewDependenceManager() {
			keyDirections	= new List<Vector3>();
			influences   	= new List<float>();
		}
		
		public void AddKeyDirection(Vector3 direction, float influence = 1.0f) {
			keyDirections.Add(direction.normalized);
			influences.Add(influence);
		}
		
		public void DeleteKeyDirection() {
			if (nKeyDirections == 0) {
				Debug.Log ("Cannot delete key view directions any more.");
				return;
			}
			keyDirections.RemoveAt(keyDirections.Count - 1);
			influences.RemoveAt(influences.Count - 1);
		}
		
		public List<float> GetWeights(Vector3 currentDirection) {
			currentDirection = currentDirection.normalized;
			
			List<float> weights = new List<float>(nKeyDirections);
			
			if (nKeyDirections == 0) {
				Debug.Log("Nothing to do.");
				return weights;
			}
							
			// note: the first direction is dummy.
			
			List<float> tmpWeights = new List<float>();
			for (int i = 1; i < nKeyDirections; ++ i) {
				Vector3 center = keyDirections[i];
				float   alpha  = influences[i];

				// phi (x) is a Gaussian function:
				// phi (x) = exp (- (ax)^2) 
				// recommended value of a is between 0.8 and 2.0
				float	cosineValue = Vector3.Dot (center, currentDirection);
				float	angle		= Mathf.Acos (cosineValue);
				float	phi			= Mathf.Exp (- ((angle * angle) / (alpha * alpha)));
				float   result		= phi;
				
				tmpWeights.Add(result);
			}
			
			float sum = 0.0f;
			for (int i = 0; i < tmpWeights.Count; ++ i) {
				sum += tmpWeights[i];
			}
			
			if (sum > 1.0f) {
				for (int i = 0; i < tmpWeights.Count; ++ i) {
					tmpWeights[i] /= sum;
				}
				sum = 1.0f;
			}
			
			weights.Add(1.0f - sum);	// the weight for default setting
			
			for (int i = 0; i < tmpWeights.Count; ++ i) {
				weights.Add(tmpWeights[i]);
			}
			return weights;
		}	
	}
}
