using UnityEngine;
using System.Collections.Generic;

namespace ViewDependentRodSimulation {
	
	[System.Serializable]
	public class Cluster {
		
		[SerializeField] private	RodObject	obj;
		
		public	float	mass;
		public	Vector3	c;
		public	Vector3	c0;
		public	Matrix3x3	Eigen;
		public	Matrix3x3	R;
		
		public	List<int>	particleIndices;
		
		public Cluster(RodObject obj) {
			this.obj	= obj;
			
			particleIndices = new List<int>();
		}
		
		public void Initialize() {
			Eigen	= Matrix3x3.identity;
			R		= Matrix3x3.identity;
			
			CalculateSumOfMass();
			CalculateRestCenterPosition();
		}
		
		void CalculateSumOfMass() {
			float sum = 0.0f;
			foreach (int particleIndex in particleIndices) {
				sum += obj.particles[particleIndex].m;
			}
			mass = sum;
		}
		
		void CalculateRestCenterPosition() {
			Vector3 sum = Vector3.zero;
			foreach (int particleIndex in particleIndices) {
				sum += obj.particles[particleIndex].x0 * obj.particles[particleIndex].m;
			}
			sum /= mass;
			c0 = sum;
		}
		
		public void UpdateCurrentCenterPosition() {
			Vector3 sum = Vector3.zero;
			foreach (int particleIndex in particleIndices) {
				sum += obj.particles[particleIndex].xp * obj.particles[particleIndex].m;
			}
			sum /= mass;
			c = sum;
		}
		
		public Matrix3x3 GetMomentMatrix() {
		    Matrix3x3 mcc0t;
			
			mcc0t.m00 = mass * c.x * c0.x;
			mcc0t.m01 = mass * c.x * c0.y;
			mcc0t.m02 = mass * c.x * c0.z;
			mcc0t.m10 = mass * c.y * c0.x;
			mcc0t.m11 = mass * c.y * c0.y;
			mcc0t.m12 = mass * c.y * c0.z;
			mcc0t.m20 = mass * c.z * c0.x;
			mcc0t.m21 = mass * c.z * c0.y;
			mcc0t.m22 = mass * c.z * c0.z;
			
		    Matrix3x3 sum = Matrix3x3.zero;
			foreach (int particleIndex in particleIndices) {
				sum += obj.particles[particleIndex].mxx0t;
				sum += obj.particles[particleIndex].Ae;
			}
		    Matrix3x3 sumMinusMcc0t = sum - mcc0t;
		    return sumMinusMcc0t;
		}
	}
}