using UnityEngine;
using System.Collections.Generic;
using System;

namespace ViewDependentRodSimulation {
	
	public class SuppressedRodObject : ExampleBasedRodObject {
		
		public override void AddPoseEditor() {
			base.AddPoseEditor();
		}
		
		public override void DeletePoseEditor() {
			base.DeletePoseEditor();
		}
		
		/******************************************************************/
		
		// R ^ (nPoses - 1) * (4 * (nParticles - 1))
		// in the paper, this corresponds to (L * L^t)^(-1) * L^t
		protected MatrixVL staticMatrixForProjection;

		private	float blend			= 0.01f;
		private	bool  suppression 	= false;

		public override void AddPose() {
			base.AddPose();
			
			minWeights.Add(0.0f);
			maxWeights.Add(0.0f);
			weightsForVelocity.Add(0.0f);
			
			UpdateStaticMatrixForProjection();
		}
		
		public override void DeletePose() {
			if (nPoses <= 1) {
				Debug.Log ("Nothing to do.");
				return;
			}
			base.DeletePose();
			
			minWeights.RemoveAt(minWeights.Count - 1);
			maxWeights.RemoveAt(maxWeights.Count - 1);
			weightsForVelocity.RemoveAt(weightsForVelocity.Count - 1);
			
			UpdateStaticMatrixForProjection();
		}
		
		protected override void Initialize() {
			minWeights			= new List<float>();
			maxWeights			= new List<float>();
			weightsForVelocity	= new List<float>();

			base.Initialize();
			
			for (int i = 0; i < nPoses; ++ i) {
				minWeights.Add(0.0f);
				maxWeights.Add(0.0f);
				weightsForVelocity.Add(0.0f);
			}
			UpdateStaticMatrixForProjection();
		}
		
		protected List<float> minWeights;
		protected List<float> maxWeights;
		protected List<float> weightsForVelocity;
		
		protected List<Vector3>			positionsForVelocity;		// for suppression
		protected List<QuaternionEx>	orientationsForVelocity;	// for suppression
		
		public override void ProjectConstraints(float dt, int stiffness) {
			
		    UpdateWeights();

			// interpolation
			VectorVL	targetDescriptor	= CalculateInterpolatedDescriptor();
			Pose		targetPose			= CalculatePoseFromDescriptor(targetDescriptor);
			
			List<QuaternionEx> targetOrientations = CalculateInterpolatedOrientations();
			
			UpdateFKParamsWithNewPose(targetPose, targetOrientations);
			UpdateRestStateOfParticlesUsingFKParams();
			
			CalculateStaticVariables();
			
			for (int i = 0; i < stiffness; ++ i) {
				ProjectCollisionConstraints(dt);
				ProjectElasticConstraints(dt);
				ProjectTorsionResistanceConstraints(dt);
				ProjectDistanceConstraints(dt);
				ProjectAnimationConstraints(dt); 
			}
			
		}
		
		public override void ApplyEstimatedPositions(float dt) {
			for (int i = 0; i < particles.Count; ++ i) {
				OrientedParticle p = particles[i];
				
				// velocity
				if (suppression) {
	            	p.v = (positionsForVelocity[i] - p.x) / dt;
				} else {
					p.v = (p.xp - p.x) / dt;
				}
				
				// position
				p.x = p.xp;
				
				// angular velocity
				QuaternionEx qp;
				if (suppression) {
					qp		= orientationsForVelocity[i];
				} else {
					qp		= p.qp;
				}
				QuaternionEx	qinv	= p.q.inverse;
				float			dot		= QuaternionEx.DotProduct(qp, qinv);
				
				if (dot < 0.0f) {
					qp = -1.0f * qp;
				}
				
				QuaternionEx	tempQ	= qp * qinv;
				Vector3			axis	= tempQ.axis;
				float			angle	= tempQ.angle;
				if (angle < 0.0001f) {
					p.w = Vector3.zero;
				} else {
					p.w = (angle / dt) * axis;
				}
				
				// orientation
				p.q = p.qp.normalize;
			}
		}
		
		protected override void UpdateWeights() {
		    if (nPoses == 1) {
				weights[0]            = 1.0f;
				minWeights[0]         = 1.0f;
				maxWeights[0]         = 1.0f;
				weightsForVelocity[0] = 1.0f;
				return;
			}
			
			base.UpdateWeights();
		}
		
		protected void UpdateMinWeights() {
			for (int i = 0; i < minWeights.Count; ++ i) {
				if (minWeights[i] > weights[i]) {
					minWeights[i] = weights[i];
				} else {
					minWeights[i] = blend * weights[i] + (1.0f - blend) * minWeights[i];
				}
			}
		}
		
		protected void UpdateMaxWeights() {
			for (int i = 0; i < maxWeights.Count; ++ i) {
				if (maxWeights[i] < weights[i]) {
					maxWeights[i] = weights[i];
				} else {
					maxWeights[i] = blend * weights[i] + (1.0f - blend) * maxWeights[i];
				}
			}
		}
		
		protected void UpdateWeightsForVelocity() {
			List<float> projected  = GetProjectedWeightsForVelocity();
			List<float> normalized = GetNormalizedWeights(projected);
			weightsForVelocity = normalized;
		}
		
		protected List<float> GetProjectedWeightsForVelocity() {
			
			// assumption: nPose >= 2 (#additional inputs >= 1)
						
			// get the current pose
			Pose currentPose = GetCurrentPose();
			
			// prepare variables for projection
			VectorVL P   = CalculateDescriptorFromPose(currentPose);
			VectorVL P_0 = descriptors[0];
			MatrixVL Q   = staticMatrixForProjection;
			
			// calculate weights except for w_0
			VectorVL projected = Q * (P - P_0);       // projected = (w_1, w_2, ..., w_n-1)

			// calculate w_0 from w_1, w_2, ... w_n-1
			float w_0 = 1.0f;
			for (int i = 0; i < projected.size; ++ i) {
				w_0 -= projected[i];
			}
   			
			// make full-length weights
			List<float> result = new List<float>();
			result.Add(w_0);
			for (int i = 0; i < projected.size; ++ i) {
				result.Add(projected[i]);
			}
			
			return result;
		}
		
		// normalize the weights using minWeights and maxWeights
		protected List<float> GetNormalizedWeights(List<float> weights) {
			
			float scale = 20.0f;
			int n = 0;
			while ((n++) < 10000) {		
				
				List<float> errors = GetErrors(weights, minWeights, maxWeights);
				
				int maxIndex = GetIndexOfMax(errors);
				
				float tmp	 = errors[maxIndex];
				float error  = tmp / scale;    		                   	// signed error
				float adjust = error / (float) (weights.Count - 1);   	// for preserving the sum == 1.0
				
				if (Mathf.Abs(tmp) <= 0.001f) {
					break;
				}
				
				for (int i = 0; i < weights.Count; ++ i) {
					if (i == maxIndex) {
						weights[i] -= error;
					} else {
						weights[i] += adjust;
					}
				}
			}
			
			return weights;
		}
		
		static private List<float> GetErrors(List<float> vec, List<float> min, List<float> max) {
			List<float> error = new List<float>(vec.Count);
			
			for (int i = 0; i < vec.Count; ++ i) {
				error.Add(GetSignedError(vec[i], min[i], max[i]));
			}
			return error;
		}
		
		static private float GetSignedError(float x, float min, float max) {
			float sub1 = x - max;
			float sub2 = x - min;
			
			bool b1 = (sub1 <= 0.0f);
			bool b2 = (sub2 >= 0.0f);
			
			if (b1) {
				if (b2) {
					return 0.0f;
				} else {
					return sub2;
				}
			} else {
				return sub1;
			}
		}
				
		// This method should be called once after a pose is added or deleted.
		protected void UpdateStaticMatrixForProjection() {
			
			// assumption: nPoses >= 2 (#additional inputs >= 1)
			if (nPoses == 1) {
				return;
			}
						
			VectorVL  P_0   = descriptors[0];
			int       pSize = P_0.size;							// pSize corresponds to "N - 1" in the paper
			
			MatrixVL Lt = new MatrixVL(nPoses - 1, pSize);
			
			for (int i = 0; i < nPoses - 1; ++ i) {
				VectorVL P_i_minus_P_0 = descriptors[i + 1] - P_0;
				
				for (int j = 0; j < pSize; ++ j) {
					Lt[i, j] = P_i_minus_P_0[j];
				}
			}
			
			MatrixVL L         = Lt.transpose;                 // pSize     , nPoses - 1
			MatrixVL LtL       = Lt * L;                       // nPoses - 1, nPoses - 1
			MatrixVL LtL_inv   = LtL.inverse;                  // nPoses - 1, nPoses - 1
			MatrixVL LtL_invLt = LtL_inv * Lt;                 // nPoses - 1, pSize
			
			staticMatrixForProjection = LtL_invLt;             // nPoses - 1, pSize
		}

		static int GetIndexOfMax(List<float> vec) {
			int   index = -1;
			float max   = -1.0f;
			
			for (int i = 0; i < vec.Count; ++ i) {
				float abs = Mathf.Abs(vec[i]);
				if (max < abs) {
					index = i;
					max   = abs;
				}
			}
			return index;
		}
	}
}