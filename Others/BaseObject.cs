using UnityEngine;
using System.Collections;

namespace ViewDependentRodSimulation {

public abstract class BaseObject : MonoBehaviour {
	
	public	bool	useGravity	=	true;

	protected	Engine	engine;

	// Use this for initialization
	protected virtual void Start () {
		engine = transform.parent.gameObject.GetComponent<Engine>() as Engine;
		if (engine == null) {
			Debug.Log ("Engine not found.");
		}
	}
	
	// Update is called once per frame
	protected virtual void Update () {
		
	}
	
	public abstract void CalculateForces(float dt);
	public abstract void CalculateVelocities(float dt);
	public abstract void DampVelocities(float dt);
	public abstract void CalculateEstimatedPositions(float dt);
	public abstract void ProjectConstraints(float dt, int stiffness);
	public abstract void ApplyEstimatedPositions(float dt);
	public abstract void CorrectVelocities(float dt);

	public abstract void FinalizeUpdate();
}
	
}