using System.Collections.Generic;

namespace ViewDependentRodSimulation {
	public struct Pose {
		public List<QuaternionEx>	qList;
		
		public int size {
			get {
				return qList.Count;
			}
		}
		
		public QuaternionEx this [int i] {
			get {
				 return qList[i];
			}
			
			set {
				qList[i] = value;
			}
		}
		
		public void Init(int size) {
			qList = new List<QuaternionEx>(size);
		}
		
		public void Init() {
			qList = new List<QuaternionEx>();
		}

		public void Add(QuaternionEx q) {
			qList.Add(q);
		}
	}
}

